from commands.command import Command
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

class Firma(Command):
    def __init__(self):
        self.args = ""
        self.help = "Dam ti predlog kako da nazoveš firmu u stilu SFRJ državnih firmi"
    async def run(self, arguments, message, store):
        await message.channel.send("Ček da mućnem glavom...")
        chromeOptions = Options()
        chromeOptions.headless = True
        driver = webdriver.Chrome(options=chromeOptions)
        driver.get("https://nothke.github.io/SocialistCompanyGenerator/")
        p_element = driver.find_element_by_id(id_="p1")
        await message.channel.send("Mogao bi da je nazoveš " + p_element.text)
