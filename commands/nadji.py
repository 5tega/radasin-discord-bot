from commands.command import Command
CHANNEL_ID = 697024939919933520

class Nadji(Command):
    def __init__(self, store, client):
        super(Nadji, self).__init__()
        self.args = "[ime_igre:slova] [broj_ljudi:broj]"
        self.help = "Pomognem ti da skupiš ljude za igranje. Možeš imati samo jedan '!nadji' aktivan odjednom. " \
                    "Pozovi ponovo da ga resetuješ. Ukucaj 'x' da obrišeš trenutno aktivan 'nadji'."

        if "nadji" not in store.keys():
            store["nadji"] = {}

    def _get_nadji(self, user_id, store):
        for nadji_post in store["nadji"].values():
            if nadji_post.author.id == user_id:
                return nadji_post
        return None

    async def run(self, arguments, message, store):
        nadji_post = self._get_nadji(message.author.id, store)
        """
        Brisanje aktivnog Nadji Posta
        """
        if arguments[0].lower() == "x":
            if nadji_post is None:
                await message.channel.send("Nemaš aktivan 'nadji'!")
            else:
                await nadji_post.delete()
                await message.channel.send("Obrisan 'nadji'.")
            return

        """
        Objavljivanje vremena
        """
        if arguments[0].lower() == "vreme":
            if nadji_post is None:
                await message.channel.send("Nemaš aktivan 'nadji'!")
                return
            await nadji_post.set_time(arguments[1], arguments[2])
            await message.channel.send(f"Vreme podešeno na {arguments[1]} u {arguments[2]}.")
            return
        """
        Brisanje starog nadji posta
        """
        if nadji_post is not None:
            await nadji_post.delete()

        """
        Pravljenje Nadji Posta
        """

        numOfPeople = int(arguments[1])
        if numOfPeople > 20:
            await message.channel.send("Mene si našao da zajebavaš! Maksimalno tražim 20 ljudi.")
            return

        nadji_message = await self.nadji_channel.send(".")
        store["nadji"][nadji_message.id] = NadjiPost(message.author, nadji_message, arguments[0], numOfPeople, store)
        await store["nadji"][nadji_message.id].update_message()
        await message.channel.send(f"Napravljen 'nadji' u kanalu {self.nadji_channel}. Gl hf!")

    async def on_client_ready(self, client):
        self.nadji_channel = client.get_channel(CHANNEL_ID)
        print(f"NADJI: Using channel {self.nadji_channel}")

class NadjiPost:
    def __init__(self, author, message, game_name, number_of_people, store):
        self.author = author
        self.message = message
        self.game_name = game_name
        self.people_needed = number_of_people
        self.people_list = []
        self.store = store
        self.time = None

    def people_accepted(self):
        return len(self.people_list)

    def satisfied(self):
        return self.people_accepted() == self.people_needed

    async def on_add_react(self, react, user):
        if self.satisfied():
            await react.remove(user)
            await user.send("Nema više mesta u tom 'nadji'!")
            return
        else:
            self.message = react.message
            await self.update_message()

    async def on_remove_react(self, message):
        self.message = message
        await self.update_message()

    async def set_time(self, day, time):
        self.time = f"{day} u {time}"
        await self.update_message()

    async def update_message(self):
        #  Get distinct users
        self.people_list = []
        pplList = "Za sada:\n"
        for react in self.message.reactions:
            async for user in react.users():
                if user not in self.people_list:
                    self.people_list.append(user)
                    pplList += f"{len(self.people_list)}. {user.name}\n"

        for i in range(len(self.people_list), self.people_needed):
            pplList += f"{i + 1}. ?\n"

        # Update message
        if self.time is None:
            timeMessage = "Autor neka objavi vreme igranja komandom '!nadji vreme [dan] [hh:mm]'."
        else:
            timeMessage = f"**Igra se u {self.time}**"

        if self.satisfied():
            await self.message.edit(
                content=f"{self.author.name} je skupio {self.people_accepted()} ljudi za **{self.game_name}**!\n{pplList}\n{timeMessage}")
        else:
            await self.message.edit(
                content=f"{self.author.name} traži {self.people_needed} ljudi za **{self.game_name}**\nFali mu još {self.people_needed - self.people_accepted()}/{self.people_needed}.\n{pplList}\nReaguj na ovu poruku da se prijaviš.\n{timeMessage}")

    async def delete(self):
        await self.message.delete()
        del self.store["nadji"][self.message.id]