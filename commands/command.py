from abc import ABC
"""
Svaka komanda treba da nasledi ovu apstraktnu klasu.
"""
class Command(ABC):
    def __init__(self):
        # Formalna definicija argumenta za ispis u helpu, npr [ime:slova] [starost:broj]
        self.args = "Default args text"
        # Opis komande za ispis u helpu
        self.help = "Default help text"

    async def run(self, arguments, message, store):
        # Logika same komande
        pass

    async def on_client_ready(self, client):
        pass