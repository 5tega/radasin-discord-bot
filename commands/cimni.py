from commands.command import Command

class Cimni(Command):
    def __init__(self):
        self.args = ""
        self.help = "Napišem mention svakome ko se prijavio na tvoj !nadji"

    def _get_nadji(self, user_id, store):
        for nadji_post in store["nadji"].values():
            if nadji_post.author.id == user_id:
                return nadji_post
        return None

    async def run(self, arguments, message, store):
        nadji_post = self._get_nadji(message.author.id, store)
        if nadji_post is None:
            await message.channel.send("Cimaj mamu! Nemaš aktivan 'nadji'.")
            return

        msgStr = "ALO!!!\n"
        if nadji_post.people_accepted() == 0:
            await message.channel.send("Cimaj mamu! Nemam koga da cimam, niko ti se nije prijavio još na !nadji")
            return
        for user in nadji_post.people_list:
            msgStr += f"<@!{user.id}> "

        await message.channel.send(msgStr)