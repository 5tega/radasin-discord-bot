# Discord Bot "Radašin" (python 3.8)
Custom-made discord bot za Discord "Стегино Царство".
Modelovan po istoimenoj eminentnoj ličnosti iz reality programa.

### Uputstvo za podešavanje projekta
0. (Preporučljivo) Napravi virtual environment i aktiviraj ga da se zavisni paketi ne bi instalirali na tvom globalnom pythonu
1. Instaliraj zavisne biblioteke pokretanjem skripte: python install.py
2. Pokreni program: python main.py

**Svaki put kada ubaciš nove biblioteke, pokreni python write_requirements.py da se ažurira requirements.txt fajl što koristi install.py. Možeš i sam da ih navedeš.**

### Uputstvo za rad na projektu
1. Nemoj koristiti Radašinov bot token za testiranje, već registruj novog bota na svom discord developer portalu da ne bi dolazilo do konflikta.