import discord
from commands.pozdrav import Pozdrav
from commands.random import Random
from commands.firma import Firma
from commands.meme import Meme
from commands.nadji import Nadji
from listeners.update_nadji import NadjiOptIn, NadjiOptOut
from commands.cimni import Cimni
"""
True - Povezuje se na inDev bota (development)
False - Povezuje se na Radašina (production)
"""
DEVELOPMENT_MODE = True
if DEVELOPMENT_MODE:
    print("Development mode")
    CMD_PREFIX = "?"
else:
    print("Production mode")
    CMD_PREFIX = "!"

#  Konfigurisanje permissiona i pristupa discord serveru
intents = discord.Intents.default()
intents.reactions = True

client = discord.Client(intents=intents, allowed_mentions=discord.AllowedMentions.all())

"""
Sluzi kao deljena memorija za komande.
Prosledjuje se svakoj komandi koja je koristi u njenom konstruktoru prilikom
navodjenja u 'commands' rečniku.
"""
store = { }

"""
Ovde je potrebno registrovati sve komande koje Radašin prepoznaje.
Ključ predstavlja ono što će korisnik kucati da bi je pozvao.
"""

activeCommands = {
    "oj" : Pozdrav(),
    "rand" : Random(),
    "firma" : Firma(),
    "meme" : Meme(),
    "nadji" : Nadji(store, client),
    "cimni" : Cimni()
}

async def run_command(arguments, message, store):
    if arguments[0].lower() == "help":
        msg = "Help:\n"
        for cmd in activeCommands.keys():
            msg += CMD_PREFIX + cmd + " " + activeCommands[cmd].args + " - " + activeCommands[cmd].help + "\n"
        await message.channel.send(msg)
        return

    if arguments[0] not in activeCommands.keys():
        await message.channel.send("A?! Nisam te skopčo'. Napiši !help da vidiš šta sve mogu.")
        return

    await activeCommands[arguments[0]].run(arguments[1:], message, store)


"""
Listeneri za dogadjaj dodavanja reakcija.
Najviši prioritet ima listener na prvom mestu, najniži na poslednjem mestu.
Reaction event se prosledjuje niz listu dokle god ga neki listener ne odbaci.
Ukoliko ga svi odbace, neće se obraditi uopšte.
"""
reaction_listeners = [
    NadjiOptIn(),
    NadjiOptOut()
]

async def process_reaction(data, store):
    for listener in reaction_listeners:
        if await listener.notify(data, store):
            break

"""
Callback funkcije koje zapravo daju botu funkcionalnosti.
"""
@client.event
async def on_ready():
    print('Logged in as {0.user}'.format(client))
    for cmd in activeCommands.values():
        await cmd.on_client_ready(client)

@client.event
async def on_message(message):
    if message.author.id == client.user.id:
        return

    if message.content.startswith(CMD_PREFIX):
        arguments = message.content[1:].strip().split(" ")
        try:
            await run_command(arguments, message, store)
        except Exception as e:
            print(e)
            await message.channel.send("Doživeo sam moždani udar obradjujući tvoj zahtev, zovi Stegu. Probaj možda kasnije.")

@client.event
async def on_reaction_add(reaction, user):
    await process_reaction({
        "reaction": reaction,
        "user": user
    }, store)

@client.event
async def on_raw_reaction_remove(payload):
    channel = await client.fetch_channel(payload.channel_id)
    msg = await channel.fetch_message(payload.message_id)
    await process_reaction({ "message": msg }, store)

"""
Povezivanje na discord server
NEMOJ KORISTITI RADASINOV TOKEN ZA POVEZIVANJE TOKOM TESTIRANJA LOKALNOG KODA
Napravi novog bota na svom discord developer portalu i poveži se s tim tokenom
da ne bi dolazilo do konflikta.
"""
# Radašin
production_token = "NTE4MDg5MTEzMzc1NjA0NzM3.XAFaFg.sHAgQeDDKqs_clESp-UFzqrHEIk"
# InDev Radašin
development_token = "NTIyODM4MjY1NjgyODUzODg4.XBKhFA.7gS-e1xPdhwVI7tOm0A_Qx3FNSQ"

print("Connecting to Discord servers...")
if DEVELOPMENT_MODE:
    client.run(development_token)
else:
    client.run(production_token)
